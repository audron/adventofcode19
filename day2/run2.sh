#!/usr/bin/env bash

INPUT="1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,6,19,1,19,5,23,2,13,23,27,1,10,27,31,2,6,31,35,1,9,35,39,2,10,39,43,1,43,9,47,1,47,9,51,2,10,51,55,1,55,9,59,1,59,5,63,1,63,6,67,2,6,67,71,2,10,71,75,1,75,5,79,1,9,79,83,2,83,10,87,1,87,6,91,1,13,91,95,2,10,95,99,1,99,6,103,2,13,103,107,1,107,2,111,1,111,9,0,99,2,14,0,0"
#INPUT="1,0,0,4,99,5,6,0,99"

IFS=',' read -r -a IN <<< "$INPUT"; IFS=''

POSN=0
POSV=0
while true; do
  while true; do
    POS=0
    A=("${IN[@]}")
    A[1]=$POSN
    A[2]=$POSV
    while true; do
      if [ "${A[$POS]}" -eq 99 ]; then 
        break
      elif [ "${A[$POS]}" -eq 1 ]; then
        A[${A[$((POS + 3))]}]=$((${A[${A[$((POS + 1))]}]} + ${A[${A[$((POS + 2))]}]}))
      elif [ "${A[$POS]}" -eq 2 ]; then
        A[${A[$((POS + 3))]}]=$((${A[${A[$((POS + 1))]}]} * ${A[${A[$((POS + 2))]}]}))
      fi
      POS=$((POS + 4))
    done

    if [ "${A[0]}" -eq 19690720 ]; then
      echo "${A[1]} and ${A[2]}"
      break 2;
    fi
    
    if [ "$POSV" -eq 99 ]; then
      POSV=0
      break;
    else
      POSV=$((POSV + 1))
    fi
  done
  if [ "$POSN" -eq 100 ]; then
    POSN=0
    break;
  else
    POSN=$((POSN + 1))
  fi
done
