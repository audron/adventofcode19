#!/usr/bin/env sh

I=0
while IFS= read -r line; do
  I=$(($I + $(($line / 3 - 2))))
done < "./input.txt"

echo $I
