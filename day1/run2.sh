#!/usr/bin/env sh
set -x
I=0
while IFS= read -r line; do
  F=$((line / 3 - 2))
  VG=$F

  while true; do
    V=$((VG / 3 - 2))
    VG=$V
    if [ "$V" -le 0 ]; then
      break;
    else
      F=$((F + V))
    fi
  done

  I=$((I + F))
done < "./input.txt"

echo $I
